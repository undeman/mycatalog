<?php

/**
 *  MyCatalog plugin
 *
 *  @package Monstra
 *  @subpackage Plugins
 *  @author undeman
 *  @copyright 2014 / undeman
 *  @version 1.0.0
 *
 */
// Register plugin
Plugin::register(__file__, 'MyCatalog', __('MySQL catalog plugin', 'mycatalog'), '1.0.0', 'undeman', '', 'catalog');

// Load Stock Admin for Editor and Admin
if (Session::exists('user_role') && in_array(Session::get('user_role'), array('admin',
            'editor'))) {
    Plugin::admin('mycatalog');
}

/**
 * Add styles and javascript in frontend
 */
Javascript::add('plugins/mycatalog/lib/js/active.js', 'frontend', 3);
Javascript::add('plugins/mycatalog/lib/js/mycatalog.js', 'frontend', 4);
Stylesheet::add('plugins/mycatalog/lib/css/menu.css', 'frontend', 5);
Stylesheet::add('plugins/mycatalog/lib/css/mycatalog.css', 'frontend', 6);
Stylesheet::add('plugins/mycatalog/lib/css/media.css', 'frontend', 7);
#Action::add('theme_footer', 'cataloggetJs');

/**
 * Catalog Class
 */
class catalog extends Frontend {

    public static $ui = 'catalog';
    public static $ui_item = 'item';
    private static $parent_id;
    public static $meta = array();
    public static $categor_arr = array();
    public static $template = '';
    public static $temp = '';
    public static $item = array();

    public static function main() {
        self::$meta['title'] = __('Catalog', 'mycatalog');
        self::$meta['keywords'] = '';
        self::$meta['description'] = '';
        $uri = Uri::segments();
        if (@$uri[1] == 'item') {
            self::$temp = 'product';
            self::getCatArray();
            self::getProduct();
        } elseif (@$uri[1] == 'category' || @$uri[0] == 'catalog') {
            self::$temp = 'products';
            self::getCatArray();
            self::getProducts();
        } else {
            self::error404();
        }
    }

    /**
     * Build categories tree 
     * @param type $dataset
     * @return type
     */
    public static function mapTree($dataset) {
        $tree = array();
        foreach ($dataset as $id => &$node) {
            if (!$node['parent_id']) {
                $tree[$id] = &$node;
            } else {
                $dataset[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
        return $tree;
    }

    /**
     * Get Products
     *
     *  <code>
     *      echo catalog::getProducts(num);
     *  </code>
     *
     * @return string
     */
    public static function getProducts($num = 30) {
        $category = NULL;
        if (Uri::segment(1) === 'category') {
            $category = Uri::segment(2);
            self::$parent_id = self::getParentId($category);
            $total = ORM::for_table('products')->where('p_category', $category)->count();
        }
        $pager = ceil($total / $num);
        $lpag = 1;
        $rpag = $pager;
        $pg = (int) trim(Uri::segment(3), 'page-');
        $result = '';

        if ($pg < 1) {
            $pg = 1;
        } elseif ($pg > $pager) {
            $pg = $pager;
        }
        if (isset(self::$categor_arr[$category]['childs'])) {
            $items = ORM::for_table('category')->select_many('id', array('p_title' => 'title'), array('filename' => 'image'))
                            ->where('parent_id', $category)->find_many();
            return self::$template = View::factory('mycatalog/views/frontend/catview')
                    ->assign('items', $items)
                    ->render();
        } else {
            // page x num
            $pg = ($pg - 1) * $num;
            // if isset category select all products
            if (isset($category) && isset($pg)) {
                $items = ORM::for_table('products')->select_many('products.id', 'p_category', 'p_title', 'filename', 'title', 'p_price')
                        ->inner_join('images', 'products.id = images.item_id')
                        ->inner_join('category', 'products.p_category = category.id')
                        ->where('p_category', $category)
                        ->group_by('id')
                        ->limit($pg)
                        ->limit($num)
                        ->find_many();
                self::$meta['title'] = ORM::for_table('category')
                                ->select('title')
                                ->where('id', $category)
                                ->find_one()->title;
                if ($total > $num) {
                    $result = new View('mycatalog/views/frontend/pag');
                    $result->assign('pager', $pager)
                            ->assign('pg', $pg)
                            ->assign('lpag', $lpag)
                            ->assign('rpag', $rpag)
                            ->assign('category', $category)
                            ->render();
                }
            } else {
                $items = ORM::for_table('products')->select_many('products.id', 'p_category', 'p_title', 'filename', 'title', 'p_price')
                        ->inner_join('images', 'products.id = images.item_id')
                        ->inner_join('category', 'products.p_category = category.id')
                        ->where('p_category', $category)
                        ->group_by('id')
                        ->limit($num)
                        ->find_many();
            }
        }
        // View layout
        return self::$template = View::factory('mycatalog/views/frontend/products')
                ->assign('items', $items)
                ->assign('result', $result)
                ->render();
    }

    /**
     * Get categories array
     */
    private static function getCatArray() {
        $query = ORM::for_table('category')->select('id')->select('title')->select('parent_id')->group_by('id')->find_array();
        foreach ($query as $k => $v) {
            $array[$v['id']] = $v;
            if (0 === $v['id']) {
                $arr[] = $v['title'];
            }
        }
        catalog::$categor_arr = self::mapTree($array);
    }

    /**
     * Get Categories
     *
     *  <code>
     *      echo catalog::getCategories();
     *  </code>
     *
     * @return string
     */
    public static function getCategories() {
        if (empty(self::$categor_arr))
            self::getCatArray();
        $url = Option::get('siteurl') . self::$ui . '/category/';
        return View::factory('mycatalog/views/frontend/categories')
                        ->assign('tags', self::$categor_arr)
                        ->assign('url', $url)
                        ->render();
    }

    /**
     * Get category parent id
     * @param type $id
     * @return int
     */
    public static function getParentId($id) {
        if (is_null(self::$parent_id)) {
            $parent_id = ORM::for_table('category')->select('parent_id')->where('id', $id)->find_one();
            if ($parent_id) {
                return $parent_id->parent_id;
            }
            return NULL;
        } else
            return self::$parent_id;
    }

    public static function getBrands() {
        
    }

    /**
     * Get Breadcrumbs
     *
     *  <code>
     *      echo catalog::getBreadcrumbs();
     *  </code>
     *
     * @return string
     */
    public static function getBreadcrumbs() {
        //    $start = microtime(true);
        $pr_id = 0;
        $id = 0;
        $prod = null;
        if (Uri::segment(1) == 'category') {
            $id = (int) Uri::segment(2);
        } elseif (Uri::segment(1) == 'item') {
            $pr_id = (int) Uri::segment(2);
        }
        if ($pr_id > 0) {

            $prod = isset(self::$item['p_title']) ? self::$item['p_title'] : '';
            $id = isset(self::$item['p_category']) ? self::$item['p_category'] : 0;
        }
        $breadcrumbs = self::GetFilteredArray($id, catalog::$categor_arr);
        //   $time = microtime(true) - $start;
        //   printf('Скрипт выполнялся %.4F сек.', $time);
        return View::factory('mycatalog/views/frontend/bread')
                        ->assign('breadcrumbs', $breadcrumbs)
                        ->assign('pr_title', $prod)
                        ->render();
    }

    /**
     * Get Product
     *
     *  <code>
     *      echo catalog::getProduct();
     *  </code>
     *
     * @return string
     */
    public static function getProduct() {
        // Product id
        $id = (int) Uri::segment(2);
        if (isset($id)) {
            $items = ORM::for_table('products')->select_many('id', 'p_category', 'p_title', 'p_content', 'p_price', 'p_spec', 'exist')
                    ->where('id', $id)
                    ->find_one();
            if (is_null($items['id'])) {
                return self::error404();
            }
            self::$item = array('p_title' => $items['p_title'], 'p_category' => $items['p_category']);
            $images = ORM::for_table('images')->select('filename')->where('item_id', $id)->find_array();
            self::$meta['title'] = $items['p_title'];
            if ($items['exist'] >= 1) {
                $items['exist'] = __('In stock','mycatalog');
            } else
                $items['exist'] = __('Under the order','mycatalog');
            // View layout
            return self::$template = View::factory('mycatalog/views/frontend/product')
                    ->assign('id', $id)
                    ->assign('items', $items)
                    ->assign('images', $images)
                    ->render();
        } else {
            return self::error404();
        }
    }

    /**
     * Get Other products
     * same category
     *  <code>
     *      echo catalog::getOthers();
     *  </code>
     *
     * @return string
     */
    public static function getOthers() {
        $id = (int) Uri::segment(2);
        if (isset($id)) {
            $items = array();
            $cat = ORM::for_table('products')->select('p_category')->find_one($id);
            if ($cat) {
                $items = ORM::for_table('products')->select_many('products.id', 'p_category', 'p_title', 'p_content', 'filename')
                        ->inner_join('images', 'products.id = images.item_id')
                        ->where('p_category', $cat->p_category)
                        ->group_by('id')
                        ->order_by_expr('RAND()')
                        ->limit(3)
                        ->find_array();
            }
            return View::factory('mycatalog/views/frontend/other')->assign('items', $items)->
                            display();
        }
    }

    /**
     * Get main page
     * @param array $array
     * @return string
     */
    public static function getMainPage($array) {
        $result = array();
        $sql = '';
        $temp = '(SELECT products.id,p_category,p_title,filename,title,p_price FROM products
                INNER JOIN images ON products.id = images.item_id
                INNER JOIN category ON products.p_category = category.id ';
        $size = count($array);
        for ($i = 0; $i < $size; $i++) {
            if ($i != $size - 1) {
                $sql.=$temp . 'WHERE p_category=' . $array[$i] . '
                             GROUP BY id
                             LIMIT 4) UNION ';
            } else {
                $sql.=$temp . 'WHERE p_category=' . $array[$i] . '
                             GROUP BY id
                             LIMIT 4)';
            }
        }
        $items = ORM::for_table('products')->raw_query($sql)->find_array();
        foreach ($items as $value) {
            $result[$value['title']][] = $value;
        }
        return self::$template = View::factory('mycatalog/views/frontend/main')
                ->assign('items', $result)
                ->render();
    }

    /**
     * Sorting categories for breadcrumbs
     * @param type $matchid
     * @param type $array
     * @return type $array
     */
    private static function GetFilteredArray($matchid, $array) {
        $result = array();
        foreach ($array as $id => $childarray) {
            $filteredparents = @is_array($childarray["childs"]) ? self::GetFilteredArray($matchid, $childarray["childs"]) : array();
            if ($id == $matchid || !empty($filteredparents)) {
                $temp = array($childarray['id'] => $childarray['title']) + $filteredparents;
                $result += $temp;
            }
        }
        return $result;
    }

    /**
     * Return title
     * @return type string
     */
    public static function title() {
        return self::$meta['title'];
    }

    /**
     * 
     * @return type string
     */
    public static function content() {
        return self::$template;
    }

    /**
     * 
     * @return type string
     */
    public static function template() {
        return self::$temp;
    }

    /**
     * Display 404
     */
    private static function error404() {
        self::$template = Text::toHtml(File::getContent(STORAGE . DS . 'pages' . DS . '1.page.txt'));
        self::$meta['title'] = 'error404';
        Response::status(404);
    }

}
