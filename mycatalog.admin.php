<?php

// Admin Navigation: add new item
Navigation::add(__('Catalog', 'mycatalog'), 'content', 'mycatalog', 10);

// Add styles and scripts in Backend
Javascript::add('plugins/mycatalog/lib/js/mycatalog_backend.js', 'backend', 11);
Javascript::add('plugins/mycatalog/lib/js/html5.image.preview.js', 'backend', 12);
Stylesheet::add('plugins/mycatalog/lib/css/mycatalog_backend.css', 'backend', 2);

/**
 * MyCatalog admin class
 */
class mycatalogAdmin extends Backend {

    /**
     * Main mycatalog admin function
     */
    public static function main() {
        // Options resize
        $Resize = array(
            'width' => __('Same width', 'mycatalog'),
            'height' => __('Same height', 'mycatalog'),
            'crop' => __('Crop images', 'mycatalog'),
            'stretch' => __('Stretch images', 'mycatalog'),
        );


        // Get url
        $siteurl = Option::get('siteurl');
        // Get Dir
        $dir = ROOT . DS . 'public' . DS . 'catalog' . DS;
        // Get dir of small folder
        $small = $dir . 'small' . DS;
        // Get dir of small folder
        $large = $dir . 'large' . DS;
        // Scan images
        $files = File::scan($small, 'jpg');

        //
        // Check for get actions
        // -------------------------------------
        if (Request::get('action')) {
            switch (Request::get('action')) {
                // Add product
                case "add":
                    if (Request::post('addProdct')) {
                        if (Security::check(Request::post('csrf'))) {
                            $title = trim(Request::post('title'));
                            $cat = (Request::get('cat'));
                            if ($title) {
                                $description = Request::post('description');
                                $Price = (Request::post('price')) ? Request::post('price') : 0;
                                $specification = Request::post('specification');
                                $exist = (Request::post('exist')) ? Request::post('exist') : 0;
                                // Get function convert and apply                            
                                $query = ORM::for_table('products')->create();
                                $query->set(array(
                                    'p_category' => $cat,
                                    'p_title' => $title,
                                    'p_content' => $description,
                                    'p_price' => $Price,
                                    'p_spec' => $specification,
                                    'exist' => $exist,
                                    'brand' => Request::post('brands')
                                ));
                                $query->save();
                                if ($query) {
                                    $product_id = $query->id();
                                    $img_name = self::conVert('image');
                                    foreach ($img_name as $value) {
                                        $img = ORM::for_table('images')->create();
                                        $img->set(array(
                                            'item_id' => $product_id,
                                            'filename' => $value
                                        ));
                                        $img->save();
                                    }
                                    Notification::set('success', 'Товар успешно добавлен');
                                }
                            } else {
                                Notification::set('errors', 'Ошибка! Товар не добавлен,не все поля были заполненны');
                            }
                            Request::redirect('index.php?id=mycatalog&action=add&cat=' . $cat);
                        } else {
                            die('csrf detected!');
                        }
                    } else {
                        $query = ORM::for_table('category')->select_many('id', 'title')->find_many();
                        $catopt = array();
//                        foreach ($query as $k => $v) {
//                            $catopt[$v['id']] = $v['title'];
//                        }
                        $brand_option = array();
                        $query = ORM::for_table('brands')->select_many('id', 'name')->order_by_asc('name')->find_many();
                        foreach ($query as $k => $v) {
                            $brand_option[$v['id']] = $v['name'];
                        }
                    }
                    // View layout add
                    View::factory('mycatalog/views/backend/add')
                            ->assign('tags', $catopt)
                            ->assign('brand_option', $brand_option)
                            ->display();
                    break;


                // Edit product
                case "edit":
                    $id = (int) Request::get('uid');
                    $query = ORM::for_table('products')->select_many('p_title', 'p_content', 'p_price', 'p_spec', 'exist', 'brand')
                            ->where('id', $id)
                            ->find_one();
                    if ($query) {
                        // Init vars
                        $brand = $query->brand;
                        $title = $query->p_title;
                        $description = $query->p_content;
                        $Price = $query->p_price;
                        $specification = $query->p_spec;
                        $exist = $query->exist;
                        $brands = ORM::for_table('brands')->order_by_asc('name')->find_many();
                        foreach ($brands as $k => $v) {
                            $brands_option[$v['id']] = $v['name'];
                        }
                    } else {
                        Notification::set('errors', 'Ошибка! Товара с таким id не существует');
                        Request::redirect('index.php?id=mycatalog');
                    }
                    // Request
                    if (Request::post('editProdct')) {
                        if (Security::check(Request::post('csrf'))) {
                            $title = trim(Request::post('title'));
                            if ($title) {
                                $description = Request::post('description');
                                $Price = Request::post('price');
                                $specification = Request::post('specification');
                                $id = (int) Request::get('uid');
                                $exist = (int) Request::post('exist');
                                $query = ORM::for_table('products')->find_one($id);
                                $category = $query->p_categoory;
                                $query->set(array(
                                    'p_title' => $title,
                                    'p_content' => $description,
                                    'p_price' => $Price,
                                    'p_spec' => $specification,
                                    'exist' => $exist,
                                    'brand' => Request::post('brands')
                                ));
                                $query->save();
                                if ($query) {
                                    $query = ORM::for_table('images')->select_many('id', 'filename')->where('item_id', $id)->find_array();
                                    foreach ($query as $key => $value) {
                                        $image[$value['id']] = $value['filename'];
                                    }
                                    $image = isset($image) ? $image : array();
                                    if (!empty($_FILES['image']['name'][0])) {
                                        foreach ($image as $key => $value) {
                                            File::delete($large . $value);
                                            File::delete($small . $value);
                                            $img_id[] = (int) $key;
                                        }
                                        $img_id = isset($img_id) ? implode(',', $img_id) : null;
                                        if (!is_null($img_id)) {
                                            $result = ORM::for_table('images')->raw_execute('DELETE FROM `images` WHERE `id` IN (' . $img_id . ')');
                                        }
                                        $img_name = self::conVert('image');
                                        $img_prep = "($id, '%s')";
                                        $part = "";
                                        foreach ($img_name as $a) {
                                            if (strlen($part) > 0)
                                                $part.=",";
                                            $part.=sprintf("$img_prep", $a);
                                        }
                                        $result = ORM::for_table('images')->raw_execute('INSERT INTO `images` (`item_id`, `filename`) VALUES ' . $part);
                                    }
                                    Notification::set('success', __('Your item has been edit ', 'mycatalog'));
                                } else {
                                    Notification::set('errors', 'Ошибка! Не удалось обновить данные');
                                }
                            }
                            Request::redirect('index.php?id=mycatalog&cat' . $category);
                        } else {
                            die('csrf detected!');
                        }
                    }
                    // View layout
                    View::factory('mycatalog/views/backend/edit')
                            ->assign('title', $title)
                            ->assign('description', $description)
                            ->assign('brand', $brand)
                            ->assign('specification', $specification)
                            ->assign('Price', $Price)
                            ->assign('brand_option', $brands_option)
                            ->assign('exist', $exist)
                            ->display();
                    break;

                // resize
                case 'resize':
                    if (Request::post('resizephotos')) {
                        $largeFiles = File::scan($large, 'jpg');
                        $smallFiles = File::scan($small, 'jpg');
                        // Update options
                        Option::update(array(
                            'ms_width' => Request::post('wsmall'),
                            'ms_height' => Request::post('hsmall'),
                            'ms_wmax' => Request::post('wlarge'),
                            'ms_hmax' => Request::post('hlarge'),
                            'ms_resize' => Request::post('Resize')));
                        // If get images resize with ReSize function
                        if (count($largeFiles) > 0) {
                            foreach ($largeFiles as $item) {
                                self::ReSize($large . $item, $small . $item);
                            }
                            Notification::set('success', __('Your settings has been edit', 'mycatalog'));
                            Request::redirect('index.php?id=mycatalog&action=resize');
                        }
                    }
                    // View layout
                    View::factory('mycatalog/views/backend/resize')->assign('Resize', $Resize)->display();
                    break;

                // Get Sellers
                case 'brands':
                    // Get details of contact
                    if (Request::post('add')) {
                        $brandName = Request::post('brand');
                        $brand = ORM::for_table('brands')->create();
                        $brand->name = Request::post('brand');
                        $brand->save();
                        Request::redirect('index.php?id=mycatalog&action=brands');
                    } elseif (Request::post('editBrand')) {
                        $id = (int) Request::get('edit');
                        $query = ORM::for_table('brands')->find_one($id);
                        $query->name = Request::post('brand');
                        $query->save();
                        Request::redirect('index.php?id=mycatalog&action=brands');
                    } elseif (Request::get('edit')) {
                        $id = (int) Request::get('edit');
                        $brand_name = ORM::for_table('brands')->select('name')->find_one($id);
                        if ($brand_name) {
                            $brands = ORM::for_table('brands')->order_by_asc('name')->find_many();
                            $brand_name = $brand_name->name;
                            return View::factory('mycatalog/views/backend/brands')
                                            ->assign('brands', $brands)
                                            ->assign('brand_name', $brand_name)
                                            ->display();
                        } else {
                            Notification::set('errors', 'Ошибка! Не удалось получить данные');
                            Request::redirect('index.php?id=mycatalog&action=brands');
                        }
                    } elseif (Request::get('del')) {
                        $id = (int) Request::get('del');
                        $brand = ORM::for_table('brands')->find_one($id);
                        $brand->delete();
                        Notification::set('success', __('Your item has been delete', 'mycatalog'));
                        Request::redirect('index.php?id=mycatalog&action=brands');
                    }
                    $query = ORM::for_table('brands')->order_by_asc('name')->find_many();
                    $brand_name = '';

                    // View layout
                    View::factory('mycatalog/views/backend/brands')
                            ->assign('brands', $query)
                            ->display();
                    break;

                // Get category
                case 'cat':
                    $cat_id = null;
                    $cat_title = '';
                    $catImage = null;
                    $array = array();
                    if (Request::post('addCat')) {
                        $cat = Request::post('cat');
                        $category = Request::post('category');
                        $img_name = self::conVert('image');
                        $query = ORM::for_table('category')->create();
                        $query->parent_id = $cat;
                        $query->title = $category;
                        $query->image = $img_name[0];
                        $query->save();
                        Request::redirect('index.php?id=mycatalog&action=cat');
                    } elseif (Request::post('editCat')) {
                        $id = Request::get('edit');
                        $cat = Request::post('cat');
                        $cat_id = Request::post('id');
                        $category = Request::post('category');
                        $img_name = self::conVert('image');
                        $query = ORM::for_table('category')->find_one($id);
                        $query->parent_id = $cat;
                        $query->title = $category;
                        if ($img_name) {
                            $query->image = $img_name[0];
                        }
                        $query->save();
                        Request::redirect('index.php?id=mycatalog&action=cat');
                    } elseif (Request::get('edit')) {
                        $id = Request::get('edit');
                        $query = ORM::for_table('category')->find_one($id);
                        if ($query) {
                            $cat_id = $query->parent_id;
                            $cat_title = $query->title;
                            $catImage[0] = $query->image;
                        } else {
                            Request::redirect('index.php?id=mycatalog&action=cat');
                        }
                    } elseif (Request::get('del')) {
                        $id = Request::get('del');
                        $cat_image = ORM::for_table('category')->select('image')->where('id', $id)->find_one();
                        if ($cat_image) {
                            $cat_image = $cat_image->image;
                            File::delete($small . $cat_image);
                            File::delete($large . $cat_image);
                            $query = ORM::for_table('category')->find_one($id);
                            if ($query) {
                                $query->delete();
                                Notification::set('success', __('Your item has been delete ', 'mycatalog'));
                            }
                        }
                        Request::redirect('index.php?id=mycatalog&action=cat');
                    }
                    $query = ORM::for_table('category')->select_many('id', 'title', 'parent_id', 'image')->group_by('id')->find_array();
                    $catopt[0] = 'Нет';
                    foreach ($query as $k => $v) {
                        $array[$v['id']] = $v;
                        $catopt[$v['id']] = $v['title'];
                    }
                    $ts = catalog::mapTree($array);

                    // View layout
                    View::factory('mycatalog/views/backend/categories')
                            ->assign('ts', $ts)
                            ->assign('catopt', $catopt)
                            ->assign('cat_id', $cat_id)
                            ->assign('cat_title', $cat_title)
                            ->assign('catImage', $catImage)
                            ->display();
                    break;
                case 'stats':
                    $query = ORM::for_table('category')->select('title')->select_expr('COUNT(products.id)', 'number')
                            ->inner_join('products', 'category.id = products.p_category')
                            ->group_by('category.id')
                            ->order_by_asc('title')
                            ->find_many();
                    View::factory('mycatalog/views/backend/stats')->assign('stats', $query)->display();
                    break;
            }
        } else {
            // Delete product database item and images from folder
            if (Request::get('delProdct')) {
                $id = Request::get('delProdct');
                if ($id) {
                    $query = ORM::for_table('images')->select('filename')->where('item_id', $id)->find_many();
                    foreach ($query as $fname) {
                        File::delete($small . $fname['filename']);
                        File::delete($large . $fname['filename']);
                    }
                    $query = ORM::for_table('products')
                            ->raw_execute('DELETE FROM products,images USING products LEFT JOIN images ON products.id = images.item_id WHERE products.id=:id', array('id' => $id));
                    Notification::set('success', __('Your item has been delete', 'mycatalog'));
                } else {
                    Notification::set('errors', __('Error', 'mycatalog'));
                }
                Request::redirect($_SERVER['HTTP_REFERER']);
            }
            $id = 0;
            $catopt = array();
            $cid = '';
            if (Request::get('cat')) {
                $id = (int) Request::get('cat');
                $cid = '&cat=' . Request::get('cat');
            }
            $query = ORM::for_table('category')->select_many('id', 'title')->where('parent_id', $id)->find_many();
            if ($query) {
                foreach ($query as $k => $v) {
                    $catopt[$v['id']] = $v['title'];
                }
                $query = array();
            } else {
                $query = ORM::for_table('products')->select_many('products.id', 'p_category', 'p_content', 'p_title', 'filename')
                        ->left_outer_join('images', 'products.id = images.item_id')
                        ->where('p_category', $id)
                        ->group_by('id')
                        ->find_many();
            }
            // Display view
            View::factory('mycatalog/views/backend/index')
                    ->assign('ms', $catopt)
                    ->assign('cid', $cid)
                    ->assign('product', $query)
                    ->display();
        }
    }

    /**
     * function to convert
     * catalogconVert('file','name');
     * If not get image from folder get image no-preview
     * and resize this
     * @param  string $image, $name
     */
    private static function conVert($image) {

        $dir = ROOT . DS . 'public' . DS . 'catalog' . DS;
        $small = $dir . 'small' . DS;
        $large = $dir . 'large' . DS;

        // File dir from image no-preview
        $noFile = PLUGINS . DS . 'mycatalog' . DS . 'lib' . DS . 'img' . DS . 'nopreview.jpg';
        $img_arr = array();
        if (!empty($_FILES[$image]['name'][0])) {
            foreach ($_FILES[$image]['name'] as $k => $f) {
                if ($_FILES[$image]['name'][$k]) {
                    if ($_FILES[$image]['type'][$k] == 'image/jpeg' ||
                            $_FILES[$image]['type'][$k] == 'image/png' ||
                            $_FILES[$image]['type'][$k] == 'image/gif') {
                        $wx = Option::get('ms_wmax');
                        $hx = Option::get('ms_hmax');
                        $w = Option::get('ms_width');
                        $h = Option::get('ms_height');
                        $re = Option::get('ms_resize');
                        $ra = $w / $h;
                        $img = Image::factory($_FILES[$image]['tmp_name'][$k]);
                        if ($img->width > $wx or $img->height > $hx) {
                            if ($img->height > $img->width) {
                                $img->resize($wx, $hx, Image::HEIGHT);
                            } else {
                                $img->resize($wx, $hx, Image::WIDTH);
                            }
                        }
                        $name = 'img_' . Text::random('hexdec') . '.png';
                        $img->save($large . $name, 60);
                        switch ($re) {
                            case 'width' :$img->resize($w, $h, Image::WIDTH);
                                break;
                            case 'height' :$img->resize($w, $h, Image::HEIGHT);
                                break;
                            case 'stretch' :$img->resize($w, $h);
                                break;
                            case 'crop':
                                if (($img->width / $img->height) > $ra) {
                                    $img->resize($w, $h, Image::HEIGHT)->crop($w, $h, round(($img->width - $w) / 2), 0);
                                } else {
                                    $img->resize($w, $h, Image::WIDTH)->crop($w, $h, 0, 0);
                                }
                                break;
                        }
                        $img->save($small . $name);
                    }
                } else {
                    $wx = Option::get('ms_wmax');
                    $hx = Option::get('ms_hmax');
                    $w = Option::get('ms_width');
                    $h = Option::get('ms_height');
                    $re = Option::get('ms_resize');
                    $ra = $w / $h;
                    $img = Image::factory($noFile);
                    if ($img->width > $wx or $img->height > $hx) {
                        if ($img->height > $img->width) {
                            $img->resize($wx, $hx, Image::HEIGHT);
                        } else {
                            $img->resize($wx, $hx, Image::WIDTH);
                        }
                    }
                    $img->save($large . $name);
                    switch ($re) {
                        case 'width' :$img->resize($w, $h, Image::WIDTH);
                            break;
                        case 'height' :$img->resize($w, $h, Image::HEIGHT);
                            break;
                        case 'stretch' :$img->resize($w, $h);
                            break;
                        case 'crop':
                            if (($img->width / $img->height) > $ra) {
                                $img->resize($w, $h, Image::HEIGHT)->crop($w, $h, round(($img->width - $w) / 2), 0);
                            } else {
                                $img->resize($w, $h, Image::WIDTH)->crop($w, $h, 0, 0);
                            }
                            break;
                    }
                    $img->save($small . $name);
                }
                $img_arr[] = $name;
            }
            return $img_arr;
        } else {
            return false;
        }
    }

    /**
     * function to convert
     * catalogupdateConvert('file','name');
     * @param  string $image, $name
     */
    private static function updateConvert($image, $name) {
        $dir = ROOT . DS . 'public' . DS . 'catalog' . DS;
        $small = $dir . 'small' . DS;
        $large = $dir . 'large' . DS;
        if ($_FILES[$image]['name']) {
            if ($_FILES[$image]['type'] == 'image/jpeg' ||
                    $_FILES[$image]['type'] == 'image/png' ||
                    $_FILES[$image]['type'] == 'image/gif') {
                $wx = Option::get('ms_wmax');
                $hx = Option::get('ms_hmax');
                $w = Option::get('ms_width');
                $h = Option::get('ms_height');
                $re = Option::get('ms_resize');
                $ra = $w / $h;
                $img = Image::factory($_FILES[$image]['tmp_name']);
                if ($img->width > $wx or $img->height > $hx) {
                    if ($img->height > $img->width) {
                        $img->resize($wx, $hx, Image::HEIGHT);
                    } else {
                        $img->resize($wx, $hx, Image::WIDTH);
                    }
                }
                $img->save($large . $name);
                switch ($re) {
                    case 'width' :$img->resize($w, $h, Image::WIDTH);
                        break;
                    case 'height' :$img->resize($w, $h, Image::HEIGHT);
                        break;
                    case 'stretch' :$img->resize($w, $h);
                        break;
                    case 'crop':
                        if (($img->width / $img->height) > $ra) {
                            $img->resize($w, $h, Image::HEIGHT)->crop($w, $h, round(($img->width - $w) / 2), 0);
                        } else {
                            $img->resize($w, $h, Image::WIDTH)->crop($w, $h, 0, 0);
                        }
                        break;
                }
                $img->save($small . $name);
            }
        }
    }

    /**
     * function to resize
     * catalogReSize('largeImages','smallImages');
     * @param  string $largeImages, $smallImages
     */
    private static function ReSize($largeImages, $smallImages) {
        $img = Image::factory($largeImages);
        $wx = Option::get('ms_wmax');
        $hx = Option::get('ms_hmax');
        $w = Option::get('ms_width');
        $h = Option::get('ms_height');
        $re = Option::get('ms_resize');
        $ra = $w / $h;
        if ($img->width > $wx or $img->height > $hx) {
            if ($img->height > $img->width) {
                $img->resize($wx, $hx, Image::HEIGHT);
            } else {
                $img->resize($wx, $hx, Image::WIDTH);
            }
        }
        $img->save($largeImages);
        switch ($re) {
            case 'width' : $img->resize($w, $h, Image::WIDTH);
                break;
            case 'height' : $img->resize($w, $h, Image::HEIGHT);
                break;
            case 'stretch' : $img->resize($w, $h);
                break;
            case 'crop':
                if (($img->width / $img->height) > $ra) {
                    $img->resize($w, $h, Image::HEIGHT)->crop($w, $h, round(($img->width - $w) / 2), 0);
                } else {
                    $img->resize($w, $h, Image::WIDTH)->crop($w, $h, 0, 0);
                }
                break;
        }
        $img->save($smallImages);
    }

}
