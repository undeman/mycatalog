(function($) {
    $(function() {

        $.js = function(el) {
            return $('[data-js=' + el + ']');
        };

        /*
         *-------------------------------------
         * Data-text
         * Example:
         * <a data-js="img" data-text="image.jpg">get</a>
         *
         *-------------------------------------
         */
        $.js('img').on('click', function() {
            img = $(this).data('img');
            // Append data in div id data
            $('#data').html('<img src="' + img + '"/>');
            // Show Modal
            $('#modal').modal();
        });

        /*
         *-------------------------------------
         * Data-text
         * Example:
         * <a data-js="text" data-text="hello world">get</a>
         *
         *-------------------------------------
         */
        $.js('text').on('click', function() {
            text = $(this).data('text');
            // Append data in div id data
            $('#data').html('<p>' + text + '</p>');
            // Show Modal
            $('#modal').modal();
        });
    });
})(jQuery);

// Carousel
$(function() {
    $('#myNewitem').carousel({interval: 5000});
});