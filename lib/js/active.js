$(document).ready(function() {
	// Вертикальное меню
	$(".tt-menu li").hover(
		function() {
			$(this).find("ul:first").fadeIn(200);
			$(this).addClass("active");
		}, function() {
			$(this).find("ul:first").fadeOut(0);
			$(this).removeClass("active");
	});
	
	$(".tt-menu li:has(ul)").addClass("level-item");	
});