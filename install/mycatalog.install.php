<?php

defined('MONSTRA_ACCESS') or die('No direct script access.');

ORM::for_table('')->raw_execute('
DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `parent_id` int(3) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_category` int(11) NOT NULL,
  `p_title` text NOT NULL,
  `p_content` longtext NOT NULL,
  `p_price` text NOT NULL,
  `p_spec` text,
  `exist` tinyint(1) unsigned zerofill NOT NULL,
  `brand` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;');

// Options photos
Option::add('ms_width', 52);
Option::add('ms_height', 52);
Option::add('ms_wmax', 600);
Option::add('ms_hmax', 500);
Option::add('ms_resize', 'stretch');


// Folders
$dir = ROOT . DS . 'public' . DS . 'catalog' . DS;
$small = $dir . 'small' . DS;
$large = $dir . 'large' . DS;



// Create folders if is dir
Dir::create($dir);
Dir::create($small);
Dir::create($large);