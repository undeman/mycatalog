<?php defined('MONSTRA_ACCESS') or die('No direct script access.');

ORM::for_table('')->raw_execute('
DROP TABLE IF EXISTS `brands`;
DROP TABLE IF EXISTS `category`;
DROP TABLE IF EXISTS `images`;
DROP TABLE IF EXISTS `products`;');

// Delete Options photos
Option::delete('ms_width');
Option::delete('ms_height');
Option::delete('ms_wmax');
Option::delete('ms_hmax');
Option::delete('ms_resize');