<?php

if ($breadcrumbs) {
    $html = '
    <ul class="breadcrumb">
        <li><a href="' . Option::get('siteurl') . '">Каталог</a> <span class="divider">/</span></li>';
    $last_key = key(array_slice($breadcrumbs, -1, 1, TRUE));
    foreach ($breadcrumbs as $key => $value) {
        $html.='<li><a href="' . Option::get('siteurl') . catalog::$ui . '/category/' . $key . '">' . $value . '</a>';
        $html.= ($key == $last_key) ? '' : '<span class="divider">/</span></li>';
    }
    if ($pr_title) {
        $html.='<span class="divider">/</span></li><li class="active">' . $pr_title . '</li>';
    }
    $html.='
    </ul>';
    echo $html;
}
?>