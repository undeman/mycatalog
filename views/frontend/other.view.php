<?php

if (count($items) > 0) {
    $html = '<div id="otherProducts">';
    foreach ($items as $item) {
        $html .= '<div class="media">
                      <a class="pull-left" href="' . Option::get('siteurl') . catalog::$ui . '/item/' . $item['id'] . '" title="' . $item['p_title'] . '" >
                        <img class="item_tumb media-object miniTumb" src="' . Option::get('siteurl') . 'public/catalog/small/' . $item['filename'] . '"></a>
                      <div class="media-body">
                      <p class="media-heading item_name">' . $item['p_title'] . '</p>
                      <a class="" href="' . Option::get('siteurl') . catalog::$ui . '/' . catalog::$ui_item . '/' . $item['id'] . '" >  ' . __("View details", "mycatalog") . '</a>
                      </div>
                  </div>';
    }
    $html.= '</div>';
    echo $html;
}