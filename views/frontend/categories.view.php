<?php

function view_cat($dataset, $url) {
    if ($dataset > 0) {
        foreach ($dataset as $tag) {
            echo '
          <li><a href="' . Option::get('siteurl') . catalog::$ui . '/category/' . $tag['id'] . '">' . $tag["title"] . '</a>';
            if (isset($tag['childs']) > 0) {
                echo '
          <ul>';
                view_cat($tag['childs'], $url);
                echo '
          </ul>';
            }
            echo '</li>';
        }
    }
}
?>
<ul class="tt-menu">
<?php view_cat($tags, $url); ?>
</ul>