<?php

$html = '<div id="products">
                <ul class="products clearfix ">';
if (count($items) > 0) {
    foreach ($items as $key => $value) {
        $html.='<h4>' . $key . '</h4>';
        foreach ($value as $item) {
            $html.= '<li class="product">
                      <p class="item_name"><a href="' . Option::get('siteurl') . catalog::$ui . '/' . catalog::$ui_item . '/' . $item['id'] . '" title="' . $item['p_title'] . '" >' . $item['p_title'] . '</a></p>
                      <div class="ms_image">
                        <a href="' . Option::get('siteurl') . catalog::$ui . '/' . catalog::$ui_item . '/' . $item['id'] . '" title="' . $item['p_title'] . '" >
                          <img class="item_tumb" src="' . Option::get('siteurl') . 'public/catalog/large/' . $item['filename'] . '" alt="' . $item['p_title'] . '">
                        </a>
                      </div>';
            if (isset($item['p_price']))
                $html.='<div class="ms_buttons">
                      <strong>Цена:</strong>
                      <span class="item_price">' . $item['p_price'] . __(' USD','mycatalog').'.</span>
                      </div>
        </li>';
        }
    }
    $html.= '</ul></div>';

    echo $html;
}
else {
    echo '<div class="media">' . __('Still not have products', 'mycatalog') . '</div>';
}
?>
