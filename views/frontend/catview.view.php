<?php

$html = '<div id="products">
                <ul class="products clearfix ">';
if (count($items) > 0) {
    foreach ($items as $item) {
        $html.= '<li class="product">
                      <p class="item_name"><a href="' . Option::get('siteurl') . catalog::$ui . '/category/' . $item['id'] . '" title="' . $item['p_title'] . '" >' . $item['p_title'] . '</a></p>
                      <div class="ms_image">
                        <a href="' . Option::get('siteurl') . catalog::$ui . '/category/' . $item['id'] . '" title="' . $item['p_title'] . '" >
                          <img class="item_tumb" src="' . Option::get('siteurl') . 'public/catalog/large/' . $item['filename'] . '" alt="' . $item['p_title'] . '">
                        </a>
                      </div>';
    }
    $html.= '</ul></div>';

    echo $html;
}
?>
