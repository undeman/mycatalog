<?php

$pag = Request::get('pg');
// Show first page
$pagination = '<div class="pagination">
                  <ul>
                    <li><a href="' . Option::get('siteurl') . catalog::$ui . '/category/' . $category . '/page-' . $lpag . '">' . __('&lt;&lt;', 'mycatalog') . '</a></li>';
for ($i = $lpag; $i <= $rpag; $i++) {
    if ($i == $pag) {
        $pagination .= '<li class="active"><a href="' . Option::get('siteurl') . catalog::$ui . '/category/' . $category . '/page-' . $i . '">' . $i . '</a></li>';
    } else {
        $pagination .= '<li><a href="' . Option::get('siteurl') . catalog::$ui . '/category/' . $category . '/page-' . $i . '">' . $i . '</a></li>';
    }
}
// Show last page
$pagination .= '<li><a href="' . Option::get('siteurl') . catalog::$ui . '/category/' . $category . '/page-' . $rpag . '">' . __('&gt;&gt;', 'mycatalog') . '</a></li>
                </ul>
              </div>';
echo $pagination;
