<?php
if (Notification::get('success')) {
    Alert::success(Notification::get('success'));
}
if (count($items) > 0) {
    $html_img = '';
    $html_limg = '';
    $temp = 'active ';
    $temp_class = 'class="item_tumb"';
    foreach ($images as $key => $img) {
        $html_limg.='<div class="' . $temp . 'item">
           <a href="#"  data-js="img" data-img="' . Option::get('siteurl') . 'public/catalog/large/' . $img['filename'] . '" title="' . $items['p_title'] . '"  alt="' . $items['p_title'] . '">
            <img ' . $temp_class . ' src="' . Option::get('siteurl') . 'public/catalog/large/' . $img['filename'] . '"></a>
          </div>';
        $html_img.='<li data-target="#myProduct" data-slide-to="' . $key . '" class="active"><a href="#">
               <img  class="miniTumb thumbnail" src="' . Option::get('siteurl') . 'public/catalog/small/' . $img['filename'] . '">
               </a></li>';
        $temp = '';
        $temp_class = '';
    }
    $html = '<div id="product" class="clearfix">
              <h3>' . $items['p_title'] . '</h3>
              ' . Html::br(2) . '
              <div class="row-fluid">
              <div class="span8">
                <div class="well-small">
                  <div id="myProduct" class="carousel slide">
                    <div class="carousel-inner">' .
            $html_limg
            . '</div>
                </div>
                ' . Html::br(1) . '
                <div class="ms_tumbs thumbnails">
                  <ul>' . $html_img . '
                  </ul>
                </div>
                </div>
              </div>
              <b>' . __('Price', 'mycatalog') . ': ' . $items['p_price'] . __(' USD','mycatalog').'</b><br />
              <b>' . __('Existence', 'mycatalog') . ': ' . $items['exist'] . '</b>
            </div>
            <div class="clearfix"></div>
            <ul class="nav nav-tabs">
              <li class="active"><a href="#descripttion" data-toggle="tab">' . __('Description', 'mycatalog') . '</a></li>
              <li><a href="#specification" data-toggle="tab">' . __('Specification', 'mycatalog') . '</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="descripttion">' . Html::br() . $items['p_content'] . '</div>
              <div class="tab-pane" id="specification">' . Html::br() . $items['p_spec'];
    $html .= '</div>
            </div>
          </div>
        </div>';
}
echo $html;
?>
<!-- Modal -->
<div id="modal" class="modal hide fade in">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>&nbsp;&nbsp;&nbsp;</h3>
    </div>
    <div class="modal-body">
        <!-- Data show here -->
        <div id="data"></div>
    </div>
</div>