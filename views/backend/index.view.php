<div id="main_cat" class="clearfix">
    <div class="btn-group">
        <?php
        echo Html::anchor(__('Add product', 'mycatalog'), 'index.php?id=mycatalog&action=add' . $cid, array('class' => 'btn btn-small')) .
        Html::anchor(__('Photo settings', 'mycatalog'), 'index.php?id=mycatalog&action=resize', array('class' => 'btn btn-small')) .
        Html::anchor(__('Brands', 'mycatalog'), 'index.php?id=mycatalog&action=brands', array('class' => 'btn btn-small')) .
        Html::anchor(__('Categories', 'mycatalog'), 'index.php?id=mycatalog&action=cat', array('class' => 'btn btn-small')) .
        Html::anchor(__('Statistics', 'mycatalog'), 'index.php?id=mycatalog&action=stats', array('class' => 'btn btn-small'));
        ?>
    </div>
    <?php
    echo Html::br(2);
    if (Notification::get('success')) {
        Alert::success(Notification::get('success'));
    } elseif (Notification::get('errors')) {
        Alert::error(Notification::get('errors'));
    }
    ?>
    <!-- Filter -->
    <div class="input-prepend">
        <span class="add-on"><?php echo __('Filter', 'mycatalog'); ?>:</span>
        <input type="search" name="filt" id="Filter" placeholder="<?php echo __('enter filter terms', 'mycatalog'); ?>" onkeyup="filter(this, 'ProcessBody', '1')"  />
    </div>
    <table class="table table-bordered">
        <thead>
        <th><?php echo __('Categories', 'mycatalog') ?></th>
        </thead>
        <tbody  id="ProcessBody">
            <?php
            if (count($ms) > 0) {
                foreach ($ms as $row => $v) {
                    ?>
                    <tr>
                    <?php echo "<td><a href=\"index.php?id=mycatalog&amp;cat=$row\">$v</a></td>"; ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
<?php if (count($product) > 0) { ?>
        <table  id="catalog"  class="table table-bordered">    
            <tbody>
    <?php foreach ($product as $row) { ?>
                    <tr>
                        <td  class="image">
                            <img width="64" src="<?php echo Option::get('siteurl') . 'public/catalog/small/' . $row['filename']; ?>" alt="<?php echo $row['p_title']; ?>"/>
                        </td>
        <?php echo "<td>" . $row['p_title'] . "</td>"; ?>
                        <td>
                            <div class="pull-right">
                                <div class="btn-group">
                                    <?php
                                    echo Html::anchor(__('Edit', 'mycatalog'), 'index.php?id=mycatalog&action=edit&uid=' . $row['id'], array('class' => 'btn btn-small'));
                                    echo Html::anchor(__('Delete', 'mycatalog'), 'index.php?id=mycatalog&delProdct=' . $row['id'], array('class' => 'btn btn-small', 'onClick' => 'return confirmDelete(\'' . __('Are you sure', 'mycatalog') . '\')'));
                                    ?>
                                </div>
                            </div>
                        </td>
                    </tr>
    <?php } ?>
            </tbody>
        </table>
<?php } ?>
    <div id="pageNavPosition"></div>
</div>

<!-- To prevend change others styles in admin theme -->
<style type="text/css" media="screen">
    @media only screen and (max-width: 800px) {
        .cf:after{visibility:hidden;display:block;font-size:0;content:" ";clear:both;height:0;}* html .cf{zoom:1;}*:first-child+html .cf{zoom:1;}table{width:100%;border-collapse:collapse;border-spacing:0;}th,td{margin:0;vertical-align:top;}th{text-align:left;}table{display:block;position:relative;width:100%;}thead{display:block;float:left;}tbody{display:block;width:auto;position:relative;overflow-x:auto;white-space:nowrap;}thead tr{display:block;}th{display:block;text-align:right;}tbody tr{display:inline-block;vertical-align:top;}td{display:block;min-height:1.25em;text-align:left;}th{border-bottom:0;border-left:0;}td{border-left:0;border-right:0;border-bottom:0;}tbody tr{border-left:1px solid #babcbf;}th:last-child,td:last-child{border-bottom:1px solid #babcbf;}
    }
</style>


<script type="text/javascript"><!--
    // Paginate function
    var pager = new Pager('catalog', 5);
    pager.init();
    pager.showPageNav('pager', 'pageNavPosition');
    pager.showPage(1);
//--></script>