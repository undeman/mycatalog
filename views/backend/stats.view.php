<?php
echo '<div class="btn-group">' .
 Html::anchor(__('Back', 'mycatalog'), 'index.php?id=mycatalog', array('class' => 'btn btn-small')) .
 '</div>' . Html::br(2);
?>

<table class="table table-bordered">
    <thead>
        <tr>
            <th><?php echo __('Name', 'mycatalog'); ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody  id="ProcessBody">
        <?php
        foreach ($stats as $key => $value) {
            echo '<tr>'
            . '<td>' . $value['title'] . '</td>';
            echo '<td>' . $value['number'] . '</td>'
            . '</tr>';
        }
        ?>
    </tbody>
</table>
<div id="pageNavPosition"></div>
</div>

<!-- To prevend change others styles in admin theme -->
<style type="text/css" media="screen">
    @media only screen and (max-width: 800px) {
        .cf:after{visibility:hidden;display:block;font-size:0;content:" ";clear:both;height:0;}* html .cf{zoom:1;}*:first-child+html .cf{zoom:1;}table{width:100%;border-collapse:collapse;border-spacing:0;}th,td{margin:0;vertical-align:top;}th{text-align:left;}table{display:block;position:relative;width:100%;}thead{display:block;float:left;}tbody{display:block;width:auto;position:relative;overflow-x:auto;white-space:nowrap;}thead tr{display:block;}th{display:block;text-align:right;}tbody tr{display:inline-block;vertical-align:top;}td{display:block;min-height:1.25em;text-align:left;}th{border-bottom:0;border-left:0;}td{border-left:0;border-right:0;border-bottom:0;}tbody tr{border-left:1px solid #babcbf;}th:last-child,td:last-child{border-bottom:1px solid #babcbf;}
    }
</style>