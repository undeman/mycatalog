<div id="main_cat" class="clearfix">
    <?php
    if (Notification::get('success')) {
        Alert::success(Notification::get('success'));
    } elseif (Notification::get('errors')) {
        Alert::error(Notification::get('errors'));
    }
    echo (
    Form::open(null, array('enctype' => 'multipart/form-data')) .
    '<div class="btn-group">' .
    Form::submit('addProdct', __('Save', 'mycatalog'), array('class' => 'btn btn-small')) .
    Html::anchor(__('Back', 'mycatalog'), 'index.php?id=mycatalog', array('class' => 'btn btn-small')) .
    '</div>' .
    Html::br(2) .
    '<div class="span5">' .
    Form::label('title', __('Title', 'mycatalog')) .
    Form::input('title', '', array('class' => 'input-block-level')) . Html::br(2) .
    Form::label('price', __('Price', 'mycatalog')) . '<div style="display:inline-block;white-space:nowrap;">' .
    '<div class="controls form-inline">' .
    Form::input('price', '', array('class' => 'input', 'style' => 'width:60%')) . Html::nbsp() .
    Form::select('brands', $brand_option, 0) . Html::nbsp() .
    '<label>
                    ' . __('In stock', 'mycatalog') . ' <input type="checkbox" name="exist" value="1"> 
                </label>
            </div></div>' . Html::br(2) .
    Form::label('description', __('Description', 'mycatalog')) .
    Form::textarea('description', '', array('class' => 'input-block-level', 'rows' => '15')) . Html::br(2) .
    Form::label('specification', __('Specification', 'mycatalog')) .
    Form::textarea('specification', '', array('class' => 'input-block-level', 'rows' => '15')) . Html::br(2) .
    Form::hidden('csrf', Security::token()) .
    '</div>' . Html::br(1) .
    '<div class="span4">
            <div class="row-fluid">
                <div class="span6">' .
    Form::file('image[]', array('class' => '', 'multiple' => '', 'accept' => 'image/*', 'onchange' => 'thumb(this.files)')) .
    '</div>
            </div>
            <div class="imagePreview"></div>
        </div>' .
    Form::close());
    ?>
</div>
<script src="<?php echo Option::get('siteurl'); ?>plugins/mycatalog/lib/js/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>