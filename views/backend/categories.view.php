<?php

function view_cat($dataset, $title = '--') {
    if ($dataset > 0) {
        foreach ($dataset as $row) {
            echo '
        <tr>            
              <td>' . $row['id'] . '</td>
              <td>' . $title . '</td>
              <td>' . $row['title'] . '</td>
              <td class="image">
                <img width="64" src="' . Option::get('siteurl') . 'public/catalog/small/' . $row['image'] . '" alt="' . $row['title'] . '" />
              </td>
           <td>
                <div class="pull-right">
                    <div class="btn-group">' .
            Html::anchor(__('Edit', 'mycatalog'), 'index.php?id=mycatalog&action=cat&edit=' . $row['id'], array('class' => 'btn btn-small')) .
            Html::anchor(__('Delete', 'mycatalog'), 'index.php?id=mycatalog&action=cat&del=' . $row['id'], array('class' => 'btn btn-small', 'onClick' => 'return confirmDelete(\'' .
                __('Are you sure', 'mycatalog') . '\')')) . '
                    </div>
                </div>
            </td>
         </tr>';
            if (isset($row['childs']) > 0) {
                view_cat($row['childs'], $row['title']);
            }
        }
    } else {
        echo __('No tags yet', 'mycatalog');
    }
}
?>
<div id="main_cat" class="clearfix">
    <?php
    if (Notification::get('success')) {
        Alert::success(Notification::get('success'));
    }
    echo Html::br(2) .
    '<div class="btn-group">' .
    Html::anchor(__('Back', 'mycatalog'), 'index.php?id=mycatalog', array('class' => 'btn btn-small')) .
    '</div>' . Html::br(2);
    ?>

    <!-- Filter -->
    <div class="input-prepend">
        <span class="add-on"><?php echo __('Filter', 'mycatalog'); ?>:</span>
        <input type="search" name="filt" id="Filter" placeholder="<?php echo __('enter filter terms', 'mycatalog'); ?>" onkeyup="filter(this, 'ProcessBody', '1')"  />
        <?php
        $txedl = __('Add', 'mycatalog');
        $sumbtx = 'addCat';
        if (Request::get('edit')) {
            $txedl = __('Update', 'mycatalog');
            $sumbtx = 'editCat';
        }
        echo (
        Form::open(null, array('class' => 'form-inline', 'enctype' => 'multipart/form-data')) .
        Form::select('cat', $catopt, $cat_id, array('class' => 'input-large')) .
        Form::input('category', $cat_title, array('class' => 'input-block-level')) .
        Form::file('image[]', $catImage, array('class' => '', 'accept' => 'image/*')) .
        Form::hidden('id_cat', Request::get('edit')) .
        Form::submit($sumbtx, $txedl, array('class' => 'btn btn-success')) .
        '<div class="modal fade hide" id="snippet">
            <div class="modal-header">
                <a class="close" data-dismiss="modal">&times;</a>
            </div>
        <div class="modal-body">
        <br>
    </div>
</div>' .
        Form::close());
        ?>
    </div>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th><?php echo __('ID', 'mycatalog'); ?></th>
                <th><?php echo __('Parent', 'mycatalog'); ?></th>
                <th><?php echo __('Name', 'mycatalog'); ?></th>
                <th><?php echo __('Image', 'mycatalog'); ?></th>
                <th></th>
            </tr>
        </thead>
        <tbody  id="ProcessBody">
            <?php
            if (count($ts) > 0) {
                view_cat($ts);
            }
            ?>
        </tbody>
    </table>
    <div id="pageNavPosition"></div>
</div>

<!-- To prevend change others styles in admin theme -->
<style type="text/css" media="screen">
    @media only screen and (max-width: 800px) {
        .cf:after{visibility:hidden;display:block;font-size:0;content:" ";clear:both;height:0;}* html .cf{zoom:1;}*:first-child+html .cf{zoom:1;}table{width:100%;border-collapse:collapse;border-spacing:0;}th,td{margin:0;vertical-align:top;}th{text-align:left;}table{display:block;position:relative;width:100%;}thead{display:block;float:left;}tbody{display:block;width:auto;position:relative;overflow-x:auto;white-space:nowrap;}thead tr{display:block;}th{display:block;text-align:right;}tbody tr{display:inline-block;vertical-align:top;}td{display:block;min-height:1.25em;text-align:left;}th{border-bottom:0;border-left:0;}td{border-left:0;border-right:0;border-bottom:0;}tbody tr{border-left:1px solid #babcbf;}th:last-child,td:last-child{border-bottom:1px solid #babcbf;}
    }
</style>