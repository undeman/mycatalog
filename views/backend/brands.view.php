<div id="main_cat" class="clearfix">
    <?php if (Notification::get('success')) Alert::success(Notification::get('success')); ?>
    <?php
      echo  Html::br(2).
    '<div class="btn-group">'.
         Html::anchor( __('Back', 'mycatalog'),'index.php?id=mycatalog',array('class' => 'btn btn-small')).
    '</div>'.Html::br(2);
     ?>

    <!-- Filter -->
    <div class="input-prepend">
        <span class="add-on"><?php echo __('Filter', 'mycatalog');?>:</span>
        <input type="search" name="filt" id="Filter" placeholder="<?php echo __('enter filter terms','mycatalog');?>" onkeyup="filter(this, 'ProcessBody', '1')"  />
        <?php
        echo
        Html::br(2).
        Form::open(null,array('class'=>'form-inline')).
        Form::input('brand',isset($brand_name) ? $brand_name : '', array('class'=>'input-block-level')).
        Form::submit(isset($brand_name) ? 'editBrand' : 'add',isset($brand_name) ? __('Update','mycatalog') : __('Add','mycatalog'), array('class' => 'btn btn-success')).        
        Form::close();
        ?>
    </div>

    <table class="table table-bordered">
    <thead>
        <tr>
            <th><?php echo __('ID','mycatalog'); ?></th>
            <th><?php echo __('Name','mycatalog'); ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody  id="ProcessBody">
        <?php if (count($brands) > 0) foreach ($brands as $row) { ?>
                <tr>
                    <td><?php echo $row['id'];?></td>
                    <td><?php echo $row['name'];?></td>
                <td>
                <div class="pull-right">
                    <div class="btn-group">
                        <?php echo
            Html::anchor(__('Edit','mycatalog'), 'index.php?id=mycatalog&action=brands&edit=' . $row['id'], array('class' => 'btn btn-small')).
            Html::anchor(__('Delete','mycatalog'), 'index.php?id=mycatalog&action=brands&del=' . $row['id'], array('class' => 'btn btn-small', 'onClick' => 'return confirmDelete(\'' .
                __('Are you sure', 'mycatalog') . '\')'));
                        ?>
                    </div>
                </div>
            </td>
         </tr>
        <?php } ?>
    </tbody>
    </table>
</div>

<!-- To prevend change others styles in admin theme -->
<style type="text/css" media="screen">
@media only screen and (max-width: 800px) {
.cf:after{visibility:hidden;display:block;font-size:0;content:" ";clear:both;height:0;}* html .cf{zoom:1;}*:first-child+html .cf{zoom:1;}table{width:100%;border-collapse:collapse;border-spacing:0;}th,td{margin:0;vertical-align:top;}th{text-align:left;}table{display:block;position:relative;width:100%;}thead{display:block;float:left;}tbody{display:block;width:auto;position:relative;overflow-x:auto;white-space:nowrap;}thead tr{display:block;}th{display:block;text-align:right;}tbody tr{display:inline-block;vertical-align:top;}td{display:block;min-height:1.25em;text-align:left;}th{border-bottom:0;border-left:0;}td{border-left:0;border-right:0;border-bottom:0;}tbody tr{border-left:1px solid #babcbf;}th:last-child,td:last-child{border-bottom:1px solid #babcbf;}
}
</style>